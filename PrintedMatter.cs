﻿using System;

public abstract class PrintedMatter

{
    protected string name;

    protected int pagesCount;

    protected string publishingHouse;

    protected int publishingYear;

    private double cost;

    public PrintedMatter(string name, int pagesCount, string publishingHouse, int publishingYear, double cost)
    {
        this.Name = name;
        this.PagesCount = pagesCount;
        this.PublishingHouse = publishingHouse;
        this.PublishingYear = publishingYear;
        this.Cost = cost;
        
    }

    protected string Name { get => name; set => name = value; }
    protected int PagesCount { get => pagesCount; set => pagesCount = value; }
    protected string PublishingHouse { get => publishingHouse; set => publishingHouse = value; }
    protected int PublishingYear { get => publishingYear; set => publishingYear = value; }
    protected double Cost { get => cost; set => cost = value; }

    protected override string ToString()
    {
        return "name: " + Name + ", pagesCount: " + PagesCount + ", publishingHouse: "
            + PublishingHouse + ", publishingYear: " + PublishingYear + "cost: " + Cost;
    }
}
