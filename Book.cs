﻿using System;

public abstract class Book : PrintedMatter

{
    private string author;

    public Book(string name, int pagesCount, string publishingHouse, int publishingYear, double cost, string author)
	{
        base(name, pagesCount, publishingHouse, publishingYear, cost);

        this.author = author;
	}

    protected string Author { get => author; set => author = value; }

    protected override string ToString()
    {
        return base.ToString() + ", author: " + Author;
    }

}
