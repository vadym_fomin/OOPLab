﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OopLab
{
    interface PrintedMatterService
    {
        List<PrintedMatter> GetAll();

        List<PrintedMatter> FindByPageDiaposone(int start, int end);

        List<PrintedMatter> FindByPublishingHouse(string publishingHouse);
    }
}
