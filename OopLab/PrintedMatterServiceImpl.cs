﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OopLab
{
    class PrintedMatterServiceImpl : PrintedMatterService
    {
        private List<PrintedMatter> storedItems;

        public PrintedMatterServiceImpl()
        {
            this.storedItems = CreateAll();
        }

        private List<PrintedMatter> CreateAll()
        {
            List<PrintedMatter> result = new List<PrintedMatter>();
            PrintedMatter printedMatter1 =  new TextBook("adventure captain nemo", 500, "raduga", 1995, 200.50, "vern", "literature");
            PrintedMatter printedMatter2 = new TextBook("spring 4 in action", 760, "luxosoft", 2016, 400.25, "josua right", "programming");
            PrintedMatter printedMatter3 = new Magazine("football", 45, "football.ua", 2017, 20.0, MagazineType.SPORTS);
            result.Add(printedMatter1);
            result.Add(printedMatter2);
            result.Add(printedMatter3);
            return result;

        }

        public List<PrintedMatter> GetAll()
        {
            return storedItems;
        }

        public List<PrintedMatter> FindByPageDiaposone(int start, int end)
        {
            List<PrintedMatter> results = new List<PrintedMatter>();
            IEnumerable<PrintedMatter> query =
                from item in storedItems
                where item.PagesCount >= start && item.PagesCount <= end
                select item;

            foreach (PrintedMatter item in query)
            {
                results.Add(item);
            }
            return results;

        }

        public List<PrintedMatter> FindByPublishingHouse(string publishingHouse)
        {
            List<PrintedMatter> results = new List<PrintedMatter>();
            IEnumerable<PrintedMatter> query =
                from item in storedItems
                where item.PublishingHouse.Contains(publishingHouse)
                select item;

            foreach (PrintedMatter item in query)
            {
                results.Add(item);
            }
            return results;
        }
    }
}
