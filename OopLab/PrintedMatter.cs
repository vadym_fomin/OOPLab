﻿using System;

public abstract class PrintedMatter

{
    protected string name;

    protected int pagesCount;

    protected string publishingHouse;

    protected int publishingYear;

    private double cost;

    public PrintedMatter(string name, int pagesCount, string publishingHouse, int publishingYear, double cost)
    {
        this.Name = name;
        this.PagesCount = pagesCount;
        this.PublishingHouse = publishingHouse;
        this.PublishingYear = publishingYear;
        this.Cost = cost;
        
    }

    public string Name { get => name; set => name = value; }
    public int PagesCount { get => pagesCount; set => pagesCount = value; }
    public string PublishingHouse { get => publishingHouse; set => publishingHouse = value; }
    public int PublishingYear { get => publishingYear; set => publishingYear = value; }
    public double Cost { get => cost; set => cost = value; }

    public override string ToString()
    {
        return "name: " + Name + ", pagesCount: " + PagesCount + ", publishingHouse: "
            + PublishingHouse + ", publishingYear: " + PublishingYear + ", cost: " + Cost;
    }
}
