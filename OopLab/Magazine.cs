﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OopLab
{

    class Magazine: PrintedMatter
    {
        private MagazineType magazineType;

        public Magazine(string name, int pagesCount, string publishingHouse, int publishingYear, double cost, MagazineType magazineType) :  base(name, pagesCount, publishingHouse, publishingYear, cost)
        {
            this.MagazineType = magazineType;
        }

        public MagazineType MagazineType { get => magazineType; set => magazineType = value; }

        public override string ToString()
        {
            return base.ToString() + ", magazine type: " + MagazineType;
        }
    }
}
