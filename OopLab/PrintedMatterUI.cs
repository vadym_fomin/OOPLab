﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OopLab
{
    class PrintedMatterUI
    {
        private PrintedMatterService printedMatterService = new PrintedMatterServiceImpl();

        private void Print(List<PrintedMatter> items)
        {
            items.ForEach(Console.WriteLine);
            Console.WriteLine();
            Console.WriteLine();
        }

        public void RenderMainView()
        {
            Console.WriteLine("Choose search option: ");
            Console.WriteLine("1. Get all");
            Console.WriteLine("2. Get by pages diapasone");
            Console.WriteLine("3. Get by publishing name");
            Console.WriteLine("4. Exit");
            int option = Int32.Parse(Console.ReadLine());

            switch (option)
            {
                case 1:
                    RenderSearchAllView();
                    break;
                case 2:
                    RenderSearchByPagesView();
                    break;
                case 3:
                    RenderSearchByPublishingHouseNameView();
                    break;
                case 4:
                    Environment.Exit(0);
                    break;
                default:
                    RenderMainView();
                    break;

            }


        }

        public void RenderSearchAllView()
        {
            Print(printedMatterService.GetAll());
            RenderMainView();
        }

        public void RenderSearchByPagesView()
        {
            try
            {
                Console.WriteLine("Input min pages count");
                int min = Int32.Parse(Console.ReadLine());
                Console.WriteLine("Input max pages count");
                int max = Int32.Parse(Console.ReadLine());
                if(min < 0 || max < 0 || max <= min){
                    throw new ArgumentException();
                }
                Print(printedMatterService.FindByPageDiaposone(min, max));
                RenderMainView();
            }catch(System.Exception e)
            {
                Console.WriteLine();
                Console.WriteLine("Illegal parameters were inputed");
                Console.WriteLine();
                RenderMainView();
            }
        }

        public void RenderSearchByPublishingHouseNameView()
        {
            Console.WriteLine("Input publishing house  name");
            string name = Console.ReadLine();
            Print(printedMatterService.FindByPublishingHouse(name));
            RenderMainView();
        }
    }
}
