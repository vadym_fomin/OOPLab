﻿using System;

public class TextBook : Book
{
    private string subject;

	public TextBook(string name, int pagesCount, string publishingHouse, int publishingYear, double cost, string author, string subject)
	{
        base(name, pagesCount, publishingHouse, publishingYear, cost, author);
        this.Subject = subject;
	}

    public string Subject { get => subject; set => subject = value; }

    protected override string ToString()
    {
        return base.ToString() + ", subject: " + Subject;
    }
}
